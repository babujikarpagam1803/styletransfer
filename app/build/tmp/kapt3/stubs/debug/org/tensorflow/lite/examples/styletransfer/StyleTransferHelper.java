package org.tensorflow.lite.examples.styletransfer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u0000 22\u00020\u0001:\u000223B5\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010#\u001a\u00020$J\u0012\u0010%\u001a\u0004\u0018\u00010 2\u0006\u0010&\u001a\u00020\'H\u0002J\"\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010*\u001a\u00020 2\u0006\u0010+\u001a\u00020\u00032\u0006\u0010,\u001a\u00020\u0003H\u0002J\u000e\u0010-\u001a\u00020$2\u0006\u0010.\u001a\u00020 J\b\u0010/\u001a\u000200H\u0002J\u000e\u00101\u001a\u00020$2\u0006\u0010.\u001a\u00020 R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u000e\"\u0004\b\u001b\u0010\u0010R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0013\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"\u00a8\u00064"}, d2 = {"Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper;", "", "numThreads", "", "currentDelegate", "currentModel", "context", "Landroid/content/Context;", "styleTransferListener", "Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper$StyleTransferListener;", "(IIILandroid/content/Context;Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper$StyleTransferListener;)V", "getContext", "()Landroid/content/Context;", "getCurrentDelegate", "()I", "setCurrentDelegate", "(I)V", "getCurrentModel", "setCurrentModel", "inputPredictTargetHeight", "inputPredictTargetWidth", "inputTransformTargetHeight", "inputTransformTargetWidth", "interpreterPredict", "Lorg/tensorflow/lite/Interpreter;", "interpreterTransform", "getNumThreads", "setNumThreads", "outputPredictShape", "", "outputTransformShape", "styleImage", "Landroid/graphics/Bitmap;", "getStyleTransferListener", "()Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper$StyleTransferListener;", "clearStyleTransferHelper", "", "getOutputImage", "output", "Lorg/tensorflow/lite/support/tensorbuffer/TensorBuffer;", "processInputImage", "Lorg/tensorflow/lite/support/image/TensorImage;", "image", "targetWidth", "targetHeight", "setStyleImage", "bitmap", "setupStyleTransfer", "", "transfer", "Companion", "StyleTransferListener", "app_debug"})
public final class StyleTransferHelper {
    private int numThreads;
    private int currentDelegate;
    private int currentModel;
    @org.jetbrains.annotations.NotNull
    private final android.content.Context context = null;
    @org.jetbrains.annotations.Nullable
    private final org.tensorflow.lite.examples.styletransfer.StyleTransferHelper.StyleTransferListener styleTransferListener = null;
    private org.tensorflow.lite.Interpreter interpreterPredict;
    private org.tensorflow.lite.Interpreter interpreterTransform;
    private android.graphics.Bitmap styleImage;
    private int inputPredictTargetWidth = 0;
    private int inputPredictTargetHeight = 0;
    private int inputTransformTargetWidth = 0;
    private int inputTransformTargetHeight = 0;
    private int[] outputPredictShape = {};
    private int[] outputTransformShape = {};
    @org.jetbrains.annotations.NotNull
    public static final org.tensorflow.lite.examples.styletransfer.StyleTransferHelper.Companion Companion = null;
    public static final int DELEGATE_CPU = 0;
    public static final int DELEGATE_GPU = 1;
    public static final int DELEGATE_NNAPI = 2;
    public static final int MODEL_INT8 = 0;
    private static final java.lang.String TAG = "Style Transfer Helper";
    
    public StyleTransferHelper(int numThreads, int currentDelegate, int currentModel, @org.jetbrains.annotations.NotNull
    android.content.Context context, @org.jetbrains.annotations.Nullable
    org.tensorflow.lite.examples.styletransfer.StyleTransferHelper.StyleTransferListener styleTransferListener) {
        super();
    }
    
    public final int getNumThreads() {
        return 0;
    }
    
    public final void setNumThreads(int p0) {
    }
    
    public final int getCurrentDelegate() {
        return 0;
    }
    
    public final void setCurrentDelegate(int p0) {
    }
    
    public final int getCurrentModel() {
        return 0;
    }
    
    public final void setCurrentModel(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final org.tensorflow.lite.examples.styletransfer.StyleTransferHelper.StyleTransferListener getStyleTransferListener() {
        return null;
    }
    
    private final boolean setupStyleTransfer() {
        return false;
    }
    
    public final void transfer(@org.jetbrains.annotations.NotNull
    android.graphics.Bitmap bitmap) {
    }
    
    public final void setStyleImage(@org.jetbrains.annotations.NotNull
    android.graphics.Bitmap bitmap) {
    }
    
    public final void clearStyleTransferHelper() {
    }
    
    private final org.tensorflow.lite.support.image.TensorImage processInputImage(android.graphics.Bitmap image, int targetWidth, int targetHeight) {
        return null;
    }
    
    private final android.graphics.Bitmap getOutputImage(org.tensorflow.lite.support.tensorbuffer.TensorBuffer output) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000b"}, d2 = {"Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper$StyleTransferListener;", "", "onError", "", "error", "", "onResult", "bitmap", "Landroid/graphics/Bitmap;", "inferenceTime", "", "app_debug"})
    public static abstract interface StyleTransferListener {
        
        public abstract void onError(@org.jetbrains.annotations.NotNull
        java.lang.String error);
        
        public abstract void onResult(@org.jetbrains.annotations.NotNull
        android.graphics.Bitmap bitmap, long inferenceTime);
    }
    
    @kotlin.Metadata(mv = {1, 7, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lorg/tensorflow/lite/examples/styletransfer/StyleTransferHelper$Companion;", "", "()V", "DELEGATE_CPU", "", "DELEGATE_GPU", "DELEGATE_NNAPI", "MODEL_INT8", "TAG", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}